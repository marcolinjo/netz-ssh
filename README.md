# Wie man mit Python SSH-Befehle auf andere Rechner absetzen kann?

## Alternaternativen

https://www.paramiko.org/
https://medium.com/featurepreneur/ssh-in-python-using-paramiko-e08fd8a039f7

    ```
    #!/usr/bin/python
    https://zetcode.com/python/paramiko/?utm_content=cmp-true

    import paramiko

    hostname = 'example.com'
    port = 22

    username = 'user7'
    password = 'passwd'

    cmd = 'uname'

    with paramiko.SSHClient() as client:

        client.load_system_host_keys()
        client.connect(hostname, port, username, password)

        (stdin, stdout, stderr) = client.exec_command(cmd)

        output = stdout.read()
        print(str(output, 'utf8'))
```
